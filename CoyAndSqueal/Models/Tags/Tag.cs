﻿using CoyAndSqueal.Models.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.Models.Tags
{
    public class Tag
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Article> Articles { get; set; }
    }
}