﻿using CoyAndSqueal.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.Models.Tags
{
    public class TagsRepository : ITagsRepository
    {
        DatabaseContext context;

        public void DeleteTag(int? id)
        {
            Tag tag = context.Tags.Find(id);
            context.Tags.Remove(tag);
            Save();
        }

        public IEnumerable<Tag> GetAllTags()
        {
            return from var in context.Tags
                   select var;
        }

        public Tag GetTagByID(int? ID)
        {
            return context.Tags.Find(ID);
        }

        public void InsertTag(Tag tag)
        {
            context.Tags.Add(tag);
            Save();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateTag(Tag tag)
        {
            context.Entry(tag).State = EntityState.Modified;
            Save();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public TagsRepository(DatabaseContext context)
        {
            this.context = context;
        }
    }
}