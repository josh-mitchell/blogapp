﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.Models.Tags
{
    public interface ITagsRepository
    {
        void InsertTag(Tag tag);
        void DeleteTag(int? id);
        void UpdateTag(Tag tag);
        void Save();
        IEnumerable<Tag> GetAllTags();
        Tag GetTagByID(int? ID);
    }
}