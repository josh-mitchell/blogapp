﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoyAndSqueal.ViewModels;

namespace CoyAndSqueal.Models.Articles
{
    public interface IArticlesRepository
    {
        void InsertArticle(Article article, string[] selectedCategories, string[] selectedTags);
        void DeleteArticle(int? id);
        void UpdateArticle(Article article, string[] selectedCategories, string[] selectedTags);
        void Save();
        IEnumerable<Article> GetAllArticles();
        Article GetArticleByID(int? ID);
        IEnumerable<AssignedCategoryData> GetAllCategories(Article article);
        IEnumerable<AssignedTagData> GetAllTags(Article article);
        IQueryable<Article> FilterList(ArticleSearchModel searchModel);

    }
}