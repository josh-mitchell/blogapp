﻿using CoyAndSqueal.Models.Tags;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoyAndSqueal.Models.Articles
{
    public class Article
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DateCreated { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DateLastEdited { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
    }
}