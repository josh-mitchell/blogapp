﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using CoyAndSqueal.DAL;
using CoyAndSqueal.Models.Tags;
using CoyAndSqueal.ViewModels;

namespace CoyAndSqueal.Models.Articles
{
    public class ArticlesRepository : IArticlesRepository
    {
        DatabaseContext context;

        public void DeleteArticle(int? id)
        {
            Article article = context.Articles.Find(id);
            context.Articles.Remove(article);
            Save();
        }

        public IEnumerable<Article> GetAllArticles()
        {
            var articles = from var in context.Articles
                           select var;
            return articles.OrderByDescending(a => a.DateCreated);
        }

        public void InsertArticle(Article article, string[] selectedCategories, string[] selectedTags)
        {
            List<Category> categories = new List<Category>();
            foreach(string category in selectedCategories)
                categories.Add(context.Categories.Find(int.Parse(category)));
            
            List<Tag> tags = new List<Tag>();
            foreach (string tag in selectedTags)
                tags.Add(context.Tags.Find(int.Parse(tag)));

            article.Categories = categories;
            article.Tags = tags;
            article.DateCreated = DateTime.Now;
            article.DateLastEdited = DateTime.Now;
            context.Articles.Add(article);
            Save();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateArticle(Article article, string[] selectedCategories, string[] selectedTags)
        {
            article.DateLastEdited = DateTime.Now;
            article = updateSelectedCategories(article, selectedCategories);
            article = updateSelectedTags(article, selectedTags);
            context.Entry(article).State = EntityState.Modified;
            Save();
        }

        public Article GetArticleByID(int? ID)
        {
            return context.Articles.Find(ID);
        }

        public IEnumerable<AssignedCategoryData> GetAllCategories(Article article)
        {
            var articleCategories = new HashSet<int>(article.Categories.Select(a => a.ID));

            return from var in context.Categories
                   select new AssignedCategoryData
                   {
                       CategoryID = var.ID,
                       Assigned = articleCategories.Contains(var.ID),
                       Name = var.Name
                   };
        }

        public IEnumerable<AssignedTagData> GetAllTags(Article article)
        {
            var articleTags = new HashSet<int>(article.Tags.Select(a => a.ID));

            return from var in context.Tags
                   select new AssignedTagData
                   {
                       TagID = var.ID,
                       Assigned = articleTags.Contains(var.ID),
                       Name = var.Name
                   };
        }

        public Article updateSelectedCategories(Article article, string[] selectedCategories)
        {
            if (selectedCategories == null)
            {
                article.Categories = new List<Category>();
                return article;
            }

            var selectedCategoriesHS = new HashSet<string>(selectedCategories);
            var articleCategories = new HashSet<int>(article.Categories.Select(c => c.ID));

            foreach(var category in context.Categories)
            {
                if (selectedCategoriesHS.Contains(category.ID.ToString()))
                {
                    if (!articleCategories.Contains(category.ID))
                    {
                        article.Categories.Add(category);
                    }
                }
                else
                {
                    if (articleCategories.Contains(category.ID))
                    {
                        article.Categories.Remove(category);
                    }
                }
            }

            return article;
        }

        public Article updateSelectedTags(Article article, string[] selectedTags)
        {
            if (selectedTags == null)
            {
                article.Tags = new List<Tag>();
                return article;
            }

            var selectedTagsHS = new HashSet<string>(selectedTags);
            var articleTags = new HashSet<int>(article.Tags.Select(c => c.ID));

            foreach (var tag in context.Tags)
            {
                if (selectedTagsHS.Contains(tag.ID.ToString()))
                {
                    if (!articleTags.Contains(tag.ID))
                    {
                        article.Tags.Add(tag);
                    }
                }
                else
                {
                    if (articleTags.Contains(tag.ID))
                    {
                        article.Tags.Remove(tag);
                    }
                }
            }

            return article;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public IQueryable<Article> FilterList(ArticleSearchModel searchModel)
        {
            var result = GetAllArticles().AsQueryable();

            if(searchModel != null)
            {
                if (searchModel.Contains != null)
                    result = result.Where(a => a.Title.Contains(searchModel.Contains) 
                    || a.Description.Contains(searchModel.Contains)
                    || a.Content.Contains(searchModel.Contains)).OrderByDescending(a => a.Title.Contains(searchModel.Contains) 
                    || a.Description.Contains(searchModel.Contains)
                    || a.Content.Contains(searchModel.Contains));
                if (searchModel.DateCreated != null)
                    result = result.Where(a => a.DateCreated == searchModel.DateCreated).OrderByDescending(a => a.DateCreated);
                if (searchModel.TagID.HasValue)
                    result = result.Where(a => a.Tags.Any(o => o.ID == searchModel.TagID)).OrderByDescending(a => a.Tags.Any(o => o.ID == searchModel.TagID));
                if (searchModel.CategoryID.HasValue)
                    result = result.Where(a => a.Categories.Any(o => o.ID == searchModel.CategoryID)).OrderByDescending(a => a.Categories.Any(o => o.ID == searchModel.CategoryID));
                if (searchModel.YearCreated.HasValue)
                    result = result.Where(a => a.DateCreated.Year == searchModel.YearCreated).OrderByDescending(a => a.DateCreated.Year);
                if (searchModel.MonthCreated.HasValue)
                    result = result.Where(a => a.DateCreated.Month == searchModel.MonthCreated).OrderByDescending(a => a.DateCreated.Month);
            }

            return result;
        }

        public ArticlesRepository(DatabaseContext context)
        {
            this.context = context;
        }
    }
}