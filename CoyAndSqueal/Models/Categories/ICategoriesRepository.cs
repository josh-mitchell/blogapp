﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.Models.Categories
{
    public interface ICategoriesRepository
    {
        void InsertCategory(Category category);
        void DeleteCategory(int? id);
        void UpdateCategory(Category category);
        void Save();
        IEnumerable<Category> GetAllCategories();
        Category GetCategoryByID(int? ID);
    }
}