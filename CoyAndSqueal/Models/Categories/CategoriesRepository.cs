﻿using CoyAndSqueal.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.Models.Categories
{
    public class CategoriesRepository : ICategoriesRepository
    {
        DatabaseContext context;

        public void DeleteCategory(int? id)
        {
            Category category = context.Categories.Find(id);
            context.Categories.Remove(category);
            Save();
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return from var in context.Categories
                   select var;
        }

        public Category GetCategoryByID(int? ID)
        {
            return context.Categories.Find(ID);
        }

        public void InsertCategory(Category category)
        {
            context.Categories.Add(category);
            Save();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateCategory(Category category)
        {
            context.Entry(category).State = EntityState.Modified;
            Save();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public CategoriesRepository(DatabaseContext context)
        {
            this.context = context;
        }
    }
}