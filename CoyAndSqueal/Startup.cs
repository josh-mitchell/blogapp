﻿using Microsoft.Owin;
using Owin;
using CoyAndSqueal.DAL;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using CoyAndSqueal.Models;

[assembly: OwinStartupAttribute(typeof(CoyAndSqueal.Startup))]
namespace CoyAndSqueal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesAndUsers();
        }

        private void createRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "joshmitchell.personal@gmail.com";

                string password = "mNIJMILBRIA97CL";

                var chkUser = userManager.Create(user, password);

                if (chkUser.Succeeded)
                    userManager.AddToRole(user.Id, "Admin");
            }

            if (!roleManager.RoleExists("Creator"))
            {
                var role = new IdentityRole();
                role.Name = "Creator";
                roleManager.Create(role);
            }
        }
    }
}
