﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoyAndSqueal.DAL;
using CoyAndSqueal.Models;
using CoyAndSqueal.Models.Articles;
using CoyAndSqueal.Models.Tags;

namespace CoyAndSqueal.Controllers
{
    [Authorize(Roles = "Admin,Creator")]
    public class ArticlesController : Controller
    {
        IArticlesRepository articlesRepository;

        public ActionResult Index()
        {
            return View(articlesRepository.GetAllArticles().ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Article article = articlesRepository.GetArticleByID(id);

            if (article == null)
                return HttpNotFound();

            return View(article);
        }

        public ActionResult Create()
        {
            Article article = new Article();
            article.Categories = new List<Category>();
            article.Tags = new List<Tag>();

            ViewBag.Categories = articlesRepository.GetAllCategories(article).ToList();
            ViewBag.Tags = articlesRepository.GetAllTags(article).ToList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Description,Content")] Article article, string[] selectedCategories, string[] selectedTags)
        {
            if (ModelState.IsValid)
            {
                articlesRepository.InsertArticle(article, selectedCategories, selectedTags);
                return RedirectToAction("Index");
            }

            return View(article);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Article article = articlesRepository.GetArticleByID(id);

            if (article == null)
                return HttpNotFound();

            ViewBag.Categories = articlesRepository.GetAllCategories(article).ToList();
            ViewBag.Tags = articlesRepository.GetAllTags(article).ToList();

            return View(article);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Description,Content")] Article article, string[] selectedCategories, string[] selectedTags)
        {
            Article articleToUpdate = articlesRepository.GetArticleByID(article.ID);

            if (ModelState.IsValid)
            {
                articleToUpdate.Title = article.Title;
                articleToUpdate.Description = article.Description;
                articleToUpdate.Content = article.Content;

                articlesRepository.UpdateArticle(articleToUpdate, selectedCategories, selectedTags);
                return RedirectToAction("Index");
            }
            ViewBag.Categories = articlesRepository.GetAllCategories(article).ToList();
            ViewBag.Tags = articlesRepository.GetAllTags(article).ToList();

            return View(article);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Article article = articlesRepository.GetArticleByID(id);

            if (article == null)
                return HttpNotFound();
            
            return View(article);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            articlesRepository.DeleteArticle(id);
            return RedirectToAction("Index");
        }

        public ArticlesController()
        {
            articlesRepository = new ArticlesRepository(new DatabaseContext());
        }
    }
}
