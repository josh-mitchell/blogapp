﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoyAndSqueal.DAL;
using CoyAndSqueal.Models;
using CoyAndSqueal.Models.Categories;

namespace CoyAndSqueal.Controllers
{
    [Authorize(Roles = "Admin,Creator")]
    public class CategoriesController : Controller
    {
        private ICategoriesRepository categoriesRepository;

        public ActionResult Index()
        {
            return View(categoriesRepository.GetAllCategories());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Category category = categoriesRepository.GetCategoryByID(id);

            if (category == null)
                return HttpNotFound();
            
            return View(category);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                categoriesRepository.InsertCategory(category);
                return RedirectToAction("Index");
            }

            return View(category);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Category category = categoriesRepository.GetCategoryByID(id);

            if (category == null)
                return HttpNotFound();
            
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                categoriesRepository.UpdateCategory(category);
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Category category = categoriesRepository.GetCategoryByID(id);

            if (category == null)
                return HttpNotFound();
            
            return View(category);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            categoriesRepository.DeleteCategory(id);
            return RedirectToAction("Index");
        }

        public CategoriesController()
        {
            categoriesRepository = new CategoriesRepository(new DatabaseContext());
        }

    }
}
