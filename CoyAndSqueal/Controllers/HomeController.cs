﻿using CoyAndSqueal.Models.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoyAndSqueal.DAL;
using System.Net;
using CoyAndSqueal.ViewModels;
using CoyAndSqueal.Models.Categories;
using CoyAndSqueal.Models.Tags;
using PagedList;

namespace CoyAndSqueal.Controllers
{
    public class HomeController : Controller
    {

        IArticlesRepository articlesRepository;
        ICategoriesRepository categoriesRepository;
        ITagsRepository tagsRepository;

        public ActionResult Index(int? page)
        {
            if (page == null)
                page = 1;

            ViewBag.Tags = tagsRepository.GetAllTags().ToList();
            ViewBag.Categories = categoriesRepository.GetAllCategories().ToList();

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(articlesRepository.GetAllArticles().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Article article = articlesRepository.GetArticleByID(id);

            if (article == null)
                return HttpNotFound();

            ViewBag.Tags = tagsRepository.GetAllTags().ToList();
            ViewBag.Categories = categoriesRepository.GetAllCategories().ToList();

            return View(article);
        }

        public ActionResult Filter(ArticleSearchModel searchModel, int? page)
        {
            if (page == null)
                page = 1;

            ViewBag.Tags = tagsRepository.GetAllTags().ToList();
            ViewBag.Categories = categoriesRepository.GetAllCategories().ToList();

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(articlesRepository.FilterList(searchModel).ToPagedList(pageNumber, pageSize));
        }

        public HomeController()
        {
            articlesRepository = new ArticlesRepository(new DatabaseContext());
            categoriesRepository = new CategoriesRepository(new DatabaseContext());
            tagsRepository = new TagsRepository(new DatabaseContext());
        }
    }
}