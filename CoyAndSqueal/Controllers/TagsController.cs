﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoyAndSqueal.DAL;
using CoyAndSqueal.Models.Tags;

namespace CoyAndSqueal.Controllers
{
    [Authorize(Roles = "Admin,Creator")]
    public class TagsController : Controller
    {
        private ITagsRepository tagsRepository;

        public ActionResult Index()
        {
            return View(tagsRepository.GetAllTags());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Tag tag = tagsRepository.GetTagByID(id);
            
            if (tag == null)
                return HttpNotFound();

            return View(tag);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] Tag tag)
        {
            if (ModelState.IsValid)
            {
                tagsRepository.InsertTag(tag);
                return RedirectToAction("Index");
            }

            return View(tag);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Tag tag = tagsRepository.GetTagByID(id);

            if (tag == null)
                return HttpNotFound();
            
            return View(tag);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Tag tag)
        {
            if (ModelState.IsValid)
            {
                tagsRepository.UpdateTag(tag);
                return RedirectToAction("Index");
            }
            return View(tag);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Tag tag = tagsRepository.GetTagByID(id);

            if (tag == null)
                return HttpNotFound();

            return View(tag);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tagsRepository.DeleteTag(id);
            return RedirectToAction("Index");
        }

        public TagsController()
        {
            tagsRepository = new TagsRepository(new DatabaseContext());
        }
    }
}
