namespace CoyAndSqueal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Article",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Content = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateLastEdited = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Article_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Article", t => t.Article_ID)
                .Index(t => t.Article_ID);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.Int(nullable: false),
                        Article_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Article", t => t.Article_ID)
                .Index(t => t.Article_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tag", "Article_ID", "dbo.Article");
            DropForeignKey("dbo.Category", "Article_ID", "dbo.Article");
            DropIndex("dbo.Tag", new[] { "Article_ID" });
            DropIndex("dbo.Category", new[] { "Article_ID" });
            DropTable("dbo.Tag");
            DropTable("dbo.Category");
            DropTable("dbo.Article");
        }
    }
}
