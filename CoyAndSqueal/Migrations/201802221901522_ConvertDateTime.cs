namespace CoyAndSqueal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertDateTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Article", "DateCreated", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Article", "DateLastEdited", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Article", "DateLastEdited", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Article", "DateCreated", c => c.DateTime(nullable: false));
        }
    }
}
