namespace CoyAndSqueal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertTagsToManyRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tag", "Article_ID", "dbo.Article");
            DropIndex("dbo.Tag", new[] { "Article_ID" });
            CreateTable(
                "dbo.TagArticle",
                c => new
                    {
                        Tag_ID = c.Int(nullable: false),
                        Article_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_ID, t.Article_ID })
                .ForeignKey("dbo.Tag", t => t.Tag_ID, cascadeDelete: true)
                .ForeignKey("dbo.Article", t => t.Article_ID, cascadeDelete: true)
                .Index(t => t.Tag_ID)
                .Index(t => t.Article_ID);
            
            DropColumn("dbo.Tag", "Article_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tag", "Article_ID", c => c.Int());
            DropForeignKey("dbo.TagArticle", "Article_ID", "dbo.Article");
            DropForeignKey("dbo.TagArticle", "Tag_ID", "dbo.Tag");
            DropIndex("dbo.TagArticle", new[] { "Article_ID" });
            DropIndex("dbo.TagArticle", new[] { "Tag_ID" });
            DropTable("dbo.TagArticle");
            CreateIndex("dbo.Tag", "Article_ID");
            AddForeignKey("dbo.Tag", "Article_ID", "dbo.Article", "ID");
        }
    }
}
