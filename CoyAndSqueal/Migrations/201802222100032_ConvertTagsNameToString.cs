namespace CoyAndSqueal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertTagsNameToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tag", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tag", "Name", c => c.Int(nullable: false));
        }
    }
}
