namespace CoyAndSqueal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertCategoryToManyRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Category", "Article_ID", "dbo.Article");
            DropIndex("dbo.Category", new[] { "Article_ID" });
            CreateTable(
                "dbo.CategoryArticle",
                c => new
                    {
                        Category_ID = c.Int(nullable: false),
                        Article_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Category_ID, t.Article_ID })
                .ForeignKey("dbo.Category", t => t.Category_ID, cascadeDelete: true)
                .ForeignKey("dbo.Article", t => t.Article_ID, cascadeDelete: true)
                .Index(t => t.Category_ID)
                .Index(t => t.Article_ID);
            
            DropColumn("dbo.Category", "Article_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Category", "Article_ID", c => c.Int());
            DropForeignKey("dbo.CategoryArticle", "Article_ID", "dbo.Article");
            DropForeignKey("dbo.CategoryArticle", "Category_ID", "dbo.Category");
            DropIndex("dbo.CategoryArticle", new[] { "Article_ID" });
            DropIndex("dbo.CategoryArticle", new[] { "Category_ID" });
            DropTable("dbo.CategoryArticle");
            CreateIndex("dbo.Category", "Article_ID");
            AddForeignKey("dbo.Category", "Article_ID", "dbo.Article", "ID");
        }
    }
}
