﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.ViewModels
{
    public class AssignedTagData
    {
        public int TagID { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}