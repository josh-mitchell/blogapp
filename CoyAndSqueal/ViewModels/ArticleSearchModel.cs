﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoyAndSqueal.ViewModels
{
    public class ArticleSearchModel
    {
        public string Contains { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? TagID { get; set; }
        public int? CategoryID { get; set; }
        public int? YearCreated { get; set; }
        public int? MonthCreated { get; set; }
    }
}